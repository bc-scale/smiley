locals {
  bucket-name = "smiley-website-${var.name-suffix}"
}

data "aws_s3_bucket" "artifact-bucket" {
  bucket = var.global-bucket-name
}

resource "aws_s3_bucket" "smiley-bucket" {
  bucket = local.bucket-name
  tags   = local.common-tags
}

resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  bucket = local.bucket-name
  policy = data.aws_iam_policy_document.smiley-bucket-policy.json
}

resource "aws_s3_bucket_website_configuration" "smiley-bucket-config" {
  bucket = aws_s3_bucket.smiley-bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }

}

resource "aws_s3_bucket_acl" "smiley-bucket-acl" {
  bucket = aws_s3_bucket.smiley-bucket.id
  acl    = "private"
}

data "aws_iam_policy_document" "smiley-bucket-policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${local.bucket-name}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.smiley.iam_arn]
    }
  }
}

resource "aws_s3_object" "smile-website-html-objects" {
  for_each     = fileset("../../s3-website/dist/", "**.html")
  bucket       = aws_s3_bucket.smiley-bucket.id
  key          = each.value
  source       = "../../s3-website/dist/${each.value}"
  etag         = filemd5("../../s3-website/dist/${each.value}")
  content_type = "text/html"
  tags         = local.common-tags
}

resource "aws_s3_object" "smile-website-js-objects" {
  for_each     = fileset("../../s3-website/dist", "js/**.js**")
  bucket       = aws_s3_bucket.smiley-bucket.id
  key          = each.value
  source       = "../../s3-website/dist/${each.value}"
  etag         = filemd5("../../s3-website/dist/${each.value}")
  content_type = "text/javascript"
  tags         = local.common-tags
}

resource "aws_s3_object" "smile-website-img-objects" {
  for_each     = fileset("../../s3-website/dist", "img/**.svg")
  bucket       = aws_s3_bucket.smiley-bucket.id
  key          = each.value
  source       = "../../s3-website/dist/${each.value}"
  etag         = filemd5("../../s3-website/dist/${each.value}")
  content_type = "image/svg+xml"
  tags         = local.common-tags
}

resource "aws_s3_object" "smile-website-css-objects" {
  for_each     = fileset("../../s3-website/dist/", "css/**.css")
  bucket       = aws_s3_bucket.smiley-bucket.id
  key          = each.value
  source       = "../../s3-website/dist/${each.value}"
  etag         = filemd5("../../s3-website/dist/${each.value}")
  content_type = "text/css"
  tags         = local.common-tags
}

