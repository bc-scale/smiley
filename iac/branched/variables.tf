variable "environment" {
  type = string
  description = "Envrionment in which stack will be deployed."
  validation {
      condition = can(regex("^dev|prod|mr[0-9]{1,3}$", var.environment))
      error_message = "A valid value for env is dev, prod or mr-xx with progressive numbers."
  }
}

variable "name-suffix" {
  type = string
  description = "Suffix name to apply to each resource."
}


variable "global-bucket-name" {
  type = string
  description = "Name of the bucket in which are hosted global resource."
  default = "smiley-artifact-global"
}

variable "alias" {
    type = string
    description = "Alias of Cloudfront distribution."
}

variable "cloudflare_hostname" {
  type = string
  description = "Name of host for CNAME record what will be created on Cloudflare which point to Cloudfront distribution."
}