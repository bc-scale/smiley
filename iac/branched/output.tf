output "website-endpoint" {
  value = aws_s3_bucket_website_configuration.smiley-bucket-config.website_endpoint
}

output "cloudfront-distribution-domain" {
  value = aws_cloudfront_distribution.smiley-distribution.domain_name
}
