
resource "aws_dynamodb_table" "smiley-table" {
  name           = "smiley-${var.name-suffix}"
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "PK"
  range_key      = "SK"

  attribute {
    name = "PK"
    type = "S"
  }

  attribute {
    name = "SK"
    type = "N"
  }

  tags = local.common-tags
}
