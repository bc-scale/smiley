data "cloudflare_zone" "xyz-zone" {
  name = "ollaw.xyz"
}
resource "cloudflare_record" "cname-record" {
  zone_id = data.cloudflare_zone.xyz-zone.id
  name    = var.cloudflare_hostname
  value   = aws_cloudfront_distribution.smiley-distribution.domain_name
  type    = "CNAME"
  ttl     = 1
  proxied = false
}
