locals {
  s3-origin-id = "smiley-bucket-origin"
}

data "aws_acm_certificate" "smiley-certificate" {
  provider = aws.east
  domain   = "*.smiley.ollaw.xyz"
  statuses = ["ISSUED"]
}

resource "aws_cloudfront_origin_access_identity" "smiley" {
  comment = "Origin Access Identity for Smiley CF Distribution"
}

resource "aws_cloudfront_distribution" "smiley-distribution" {
  origin {
    domain_name = aws_s3_bucket.smiley-bucket.bucket_regional_domain_name
    origin_id   = local.s3-origin-id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.smiley.cloudfront_access_identity_path
    }
  }

  aliases = [var.alias]

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"


  //aliases = ["mysite.example.com", "yoursite.example.com"]

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3-origin-id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }


  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = local.common-tags

  viewer_certificate {
    acm_certificate_arn            = data.aws_acm_certificate.smiley-certificate.arn
    cloudfront_default_certificate = false
    minimum_protocol_version       = "TLSv1.2_2021"
    ssl_support_method             = "sni-only"
  }
}
