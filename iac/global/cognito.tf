data "aws_acm_certificate" "issued" {
  provider = aws.east
  domain   = "*.smiley.ollaw.xyz"
  statuses = ["ISSUED"]
}

resource "aws_cognito_user_pool" "smiley-pool" {
  name = "smiley-up"
  admin_create_user_config {
    allow_admin_create_user_only = true
  }
  tags = local.common-tags
}


resource "aws_cognito_user_pool_client" "smiley-browser" {
  name                                 = "smiley-browser"
  user_pool_id                         = aws_cognito_user_pool.smiley-pool.id
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows = [
    "code",
  ]
  allowed_oauth_scopes = [
    "email",
    "openid",
    "phone",
    "profile",
  ]
  supported_identity_providers = ["COGNITO", "Google"]
}

resource "aws_cognito_user_pool_ui_customization" "smiley-ui" {
  client_id = aws_cognito_user_pool_client.smiley-browser.id

  css = ".label-customizable {font-weight: 400;}"
  //image_file = filebase64("logo.png")

  user_pool_id = aws_cognito_user_pool.smiley-pool.id
}

resource "aws_cognito_identity_pool" "smiley-ip" {
  identity_pool_name               = "smiley-ip"
  allow_unauthenticated_identities = false
  allow_classic_flow               = false

  cognito_identity_providers {
    client_id               = aws_cognito_user_pool_client.smiley-browser.id # this one need to be created with GUI
    provider_name           = aws_cognito_user_pool.smiley-pool.endpoint
    server_side_token_check = false
  }

}

resource "aws_iam_role" "smiley-authenticated-role" {
  name = "smiley-cognito-authenticated"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.smiley-ip.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "smiley-authenticated-policy" {
  name = "authenticated_policy"
  role = aws_iam_role.smiley-authenticated-role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
      ],
      "Resource": [
        "*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "lambda:InvokeFunction"
      ],
      "Resource": [
        "arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:smiley-feeling-handler*"
      ]
    }
  ]
}
EOF
}

resource "aws_cognito_identity_pool_roles_attachment" "smiley-ip-role-attachment" {
  identity_pool_id = aws_cognito_identity_pool.smiley-ip.id

  role_mapping {
    identity_provider         = "${aws_cognito_user_pool.smiley-pool.endpoint}:${aws_cognito_user_pool_client.smiley-browser.id}"
    ambiguous_role_resolution = "AuthenticatedRole"
    type                      = "Rules"

    mapping_rule {
      claim      = "isAdmin"
      match_type = "Equals"
      role_arn   = aws_iam_role.smiley-authenticated-role.arn
      value      = "paid"
    }
  }

  roles = {
    "authenticated" = aws_iam_role.smiley-authenticated-role.arn
  }
}
