[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Gitlab pipeline status](https://gitlab.com/ollaww/smiley/badges/development/pipeline.svg)](https://gitlab.com/ollaww/smiley/commits/development)

# Smiley

*In progress*

Simple webapp built on AWS to track your mood day by day, as [shown here](https://smiley.ollaw.xyz).
