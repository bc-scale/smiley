export const AWS_REGION = process.env.VUE_APP_AWS_REGION

export const AWS_LAMBDA_SUBMIT_FEELING_NAME = process.env.VUE_APP_AWS_LAMBDA_SUBMIT_FEELING_NAME

export const COGNITO_POOL_ID = process.env.VUE_APP_COGNITO_POOL_ID
export const COGNITO_IDP_DEFAULT_LOGIN = process.env.VUE_APP_COGNITO_IDP_DEFAULT_LOGIN
export const COGNITO_CLIENT_ID = process.env.VUE_APP_COGNITO_CLIENT_ID
export const COGNITO_REDIRECT_URI = process.env.VUE_APP_COGNITO_REDIRECT_URI
export const COGNITO_BASE_URL = process.env.VUE_APP_COGNITO_BASE_URL
export const COGNITO_TOKEN_ENDPOINT = `${COGNITO_BASE_URL}/oauth2/token`;
export const COGNITO_WEB_UI_URL = `${COGNITO_BASE_URL}/oauth2/authorize?client_id=${COGNITO_CLIENT_ID}&response_type=code&scope=openid&redirect_uri=${encodeURIComponent(COGNITO_REDIRECT_URI)}`

