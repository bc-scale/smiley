import { COGNITO_TOKEN_ENDPOINT, COGNITO_WEB_UI_URL, COGNITO_CLIENT_ID, COGNITO_REDIRECT_URI } from './constant'

const extractCognitoCodeFromURL = () => {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.has("code") ? urlParams.get("code") : null;
}

const getCredentialsFromCode = (code) => {
    return fetch(COGNITO_TOKEN_ENDPOINT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `grant_type=authorization_code&client_id=${COGNITO_CLIENT_ID}&code=${code}&redirect_uri=${COGNITO_REDIRECT_URI}`
    });
}

var AuthLib = {
    async manageAuthentication() {
        const code = extractCognitoCodeFromURL();
        if (code) {
            return await getCredentialsFromCode(code)
                .then(response => response.json())
                .then(response => {
                    if (Object.hasOwn(response, "id_token")) {
                        return response.id_token;
                    } else window.location.replace(COGNITO_WEB_UI_URL);
                })
        } else {
            window.location.replace(COGNITO_WEB_UI_URL);
        }
    },
    getAuthURL() {
        return COGNITO_WEB_UI_URL;
    },

}

export default AuthLib