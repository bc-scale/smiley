import { fromCognitoIdentityPool } from "@aws-sdk/credential-providers";
import { LambdaClient, InvokeCommand } from '@aws-sdk/client-lambda';
import { Buffer } from 'buffer';

import { AWS_LAMBDA_SUBMIT_FEELING_NAME, COGNITO_POOL_ID, COGNITO_IDP_DEFAULT_LOGIN, AWS_REGION } from './constant'

var LambdaLib = {
    invokeMoodLambda: async (token, mood) => {
        const client = new LambdaClient({
            region: AWS_REGION,
            credentials: fromCognitoIdentityPool({
                identityPoolId: COGNITO_POOL_ID,
                logins: {
                    [COGNITO_IDP_DEFAULT_LOGIN]: token,
                },
                clientConfig: { region: AWS_REGION }
            })
        });

        const command = new InvokeCommand({
            FunctionName: AWS_LAMBDA_SUBMIT_FEELING_NAME,
            Payload: JSON.stringify(mood)
        });

        return client.send(command)
    }
}

export default LambdaLib